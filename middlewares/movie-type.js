const { MOVIE_TYPES } = require("../constants/movie-types");

function movieTypeMiddleware(req, res, next) {
  const { type } = req.query;

  if (type && !MOVIE_TYPES.includes(type)) {
    return res
      .status(400)
      .json(
        `The type ${type} is not valid. Use one of ${MOVIE_TYPES.join(", ")}`
      );
  }

  next();
}

module.exports = movieTypeMiddleware;

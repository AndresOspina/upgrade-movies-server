## EXPRESS API STEPS

### BASE EXPRESS API
1 - Install nodemon and create a `npm run dev` script.
2 - Clean the views and routes. 
3 - Remove the error handler so it returns an error in JSON response format.
4 - Add the debug environment variable to express `"dev": "DEBUG=server:server nodemon ./bin/www"`.
5 - Create a class to use the omdb API and combine it with `axios` and `qs`.
6 - Create a `search` route and method in the class that supports a name and type.
7 - Create a `get` route and method in the class that supports id, title, type and plot.
8 - Install dotenv to use global environment variables in the project.
9 - Create middlewares for both `type` and `plot` query params.

### USING MONGO AND MONGOOSE
1 - Install MongoDB Community Edition in your computer and start the service.
2 - Install `nodemon` and create a `db.js` file inside config with the database connection functions.
3 - Create a models folder and a `Movie.js` model with the schema definition.
4 - Modify the get movie controller to return a saved movie in the DB.

#### Related to mongoose
About schema types: [https://mongoosejs.com/docs/schematypes.html]
About virtuals: [http://thecodebarbarian.com/mongoose-virtual-populate]

const TYPE_MOVIE = 'movie';
const TYPE_SERIES = 'series';
const TYPE_EPISODE = 'episode';

const MOVIE_TYPES = [TYPE_MOVIE, TYPE_SERIES, TYPE_EPISODE];

module.exports = {
  TYPE_MOVIE,
  TYPE_SERIES,
  TYPE_EPISODE,
  MOVIE_TYPES,
}
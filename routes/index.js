var express = require('express');
var router = express.Router();

router.get('/ping', (req, res, next) => {
  res.status(200).json('Server working correctly!');
});

module.exports = router;

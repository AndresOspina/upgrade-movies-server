const express = require('express');
const router = express.Router();
const movieTypeMiddleware = require('../middlewares/movie-type');

const movieControllers = require('../controllers/movies');

router.get('/', [movieTypeMiddleware], movieControllers.getById);
router.get('/search', [movieTypeMiddleware], movieControllers.searchMovie);

router.get('/all', movieControllers.getAll);
router.post('/', movieControllers.createMovie);

module.exports = router;
